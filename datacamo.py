from stegano import lsb

def main():

    #Menu
    
    while True:
        print("---------Welcome to Data Camo---------")

        print()
        print("Please select an option")
        print("-----------------------")
        print()
        print("1 - Hide a string in a photo")
        print("2 - Hide a file in a photo")
        print("3 - Reveal a hidden string")
        print()
        print("H - Help")
        print("Q - Quit")
        print()
        option = input("Selection: ") 

        if (option == "1"):
            stringInPhoto()

        elif (option == "2"):
            fileInPhoto()

        elif (option == "3"):
            revealString()

        elif (option == "H"):
            helpFunction()

        elif (option == "Q"):
            return 0

        else:
            print()
            print()
            print("Error: invalid input. Please try again")
            print()
            print()


def stringInPhoto():
    #Encrypts a string using a photo

    print()
    print()
    print()
    print("Please enter the file path for the photo you want to use")
    print()
    print("Note: Please use two backslashes C:\\Users\\user\\Desktop")
    print("Also, when entering")
    print("the file path, bear in mind that Python sets the current")
    print("working directory in the same directory that the script is")
    print("being run it.")
    print()
    print()

    while True:
        filePath = input("File Path: ")
        print()
        string = input("Please enter the string you want to hide: ")
        print()
        print("Please input the name and location of where you want")
        print("the secret file saved (i.e. './secretFile.png')")
        print()
        endLocation = input("File path: ")

        
        hiddenFile = lsb.hide(filePath, string)
        hiddenFile.save(endLocation)
        


    


def fileInPhoto():
    pass




def revealString():
    print("Please enter the file path of the file you want to reveal.")
    print()
    filePath = input("File Path: ")

    print(lsb.reveal(filePath))

        



def helpFunction():
    pass

main()
